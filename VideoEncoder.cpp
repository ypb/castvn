/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**********/
// Choon Jin Ng
// Copyright (c) 2005 ViSLAB, University of Sydney.  All rights reserved.
// Video Encoder
// Implementation

#include "VideoEncoder.hh"

/*****************************************************************************
/ Constructor:
/	buffsize: video input/output buffer size
/	format: the codec of the video. eg. "mjpeg", "h264"
/	width, height: width & height of screen
/	fps: frames per second */
VideoEncoder::VideoEncoder(int buffsize, char *format, int width, int height, int fps)
	:buffsize(buffsize), width(width), height(height), fps(fps), format(format){
	//initialize libavcodec, and register all codecs and formats
    av_register_all();
	
	video_outbuf = NULL;
	video_st = NULL;
	picture = NULL;
	img_convert_ctx = NULL;
	tmp_picture = NULL;
	fmt = NULL;
	oc = NULL;
	video_st = NULL;
}

VideoEncoder::~VideoEncoder(){
	close_video();
}

//Add a video output stream
AVStream* VideoEncoder::add_video_stream(AVFormatContext *oc, int codec_id)
{
    AVCodecContext *c;
    AVStream *st;

    st = av_new_stream(oc, 0);
    if (!st) {
        fprintf(stderr, "Could not alloc stream\n");
		return NULL;
    }
    
    c = st->codec;
    c->codec_id = (CodecID) codec_id;
	if(codec_id == CODEC_ID_MJPEG)
		c->extradata_size = 28;
    c->codec_type = AVMEDIA_TYPE_VIDEO;

    /* put sample parameters */
    c->bit_rate = 400000;
	
    /* resolution must be a multiple of two */
    c->width = width;  
    c->height = height;
    /* frames per second */
    c->time_base.den = fps;  
    c->time_base.num = 1;
    c->gop_size = 12; /* emit one intra frame every twelve frames at most */
    c->pix_fmt = PIX_FMT_YUVJ420P; /* Since ONLY YUVJ420P is supported for MJPEG output */
    if (c->codec_id == CODEC_ID_MPEG2VIDEO) {
        /* just for testing, we also add B frames */
        c->max_b_frames = 2;
    }
    if (c->codec_id == CODEC_ID_MPEG1VIDEO){
        /* needed to avoid using macroblocks in which some coeffs overflow 
           this doesnt happen with normal video, it just happens here as the 
           motion of the chroma plane doesnt match the luma plane */
        c->mb_decision=2;
    }
    /* some formats want stream headers to be seperate */
    if(!strcmp(oc->oformat->name, "mp4") || !strcmp(oc->oformat->name, "mov") || !strcmp(oc->oformat->name, "3gp"))
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
    
    return st;
}

//Allocate memory space for AVFrame
AVFrame* VideoEncoder::alloc_picture(enum PixelFormat pix_fmt)
{
    AVFrame *picture;
    uint8_t *picture_buf;
    int size;
    
    picture = avcodec_alloc_frame();
    if (!picture)
        return NULL;
    size = avpicture_get_size(pix_fmt, width, height);
    picture_buf = (uint8_t *) malloc(size);
    if (!picture_buf) {
        av_free(picture);
        return NULL;
    }
    avpicture_fill((AVPicture *)picture, picture_buf, 
                   pix_fmt, width, height);
    return picture;
}

//Open video
bool VideoEncoder::open_video(AVFormatContext *oc, AVStream *st)
{
    AVCodec *codec;
    AVCodecContext *c;

    c = st->codec;

    /* find the video encoder */
    codec = avcodec_find_encoder(c->codec_id);
    if (!codec) {
       	fprintf(stderr, "codec not found\n");
		return false;
    }

    /* open the codec */
    if (avcodec_open(c, codec) < 0) {
        fprintf(stderr, "could not open codec\n");
		return false;
    }

    /* allocate the encoded raw picture */
    picture = alloc_picture(c->pix_fmt);
    if (!picture) {
        fprintf(stderr, "Could not allocate picture\n");
		return false;
    }

    /* if the output format is not RGB565, then a temporary RGB565
       picture is needed too. It is then converted to the required
       output format */
    tmp_picture = alloc_picture(STREAM_PIX_FMT);
    if (!tmp_picture) {
		fprintf(stderr, "Could not allocate temporary picture\n");
		return false;
	}

	/* allocate converter once */
	if(!img_convert_ctx) {
	  fprintf(stderr, "Converter context already present\n");
	}
	img_convert_ctx = sws_getContext(width, height,
									 STREAM_PIX_FMT,
									 c->width, c->height,
									 c->pix_fmt, SWS_BICUBIC, NULL, NULL, NULL);
	if (!img_convert_ctx) {
	  fprintf(stderr, "Could not allocate software converter context\n");
	  return false;
	}

	return true;
}

//Main call for initializing & opening of video
bool VideoEncoder::movie_open() {
	fmt = av_guess_format(format, NULL, NULL);
	if(!fmt){
		fprintf(stderr, "Could not find suitable output format for %s.\n", format);
		return false;
	}
		
	oc = avformat_alloc_context();
	if(!oc){
		fprintf(stderr, "Memory error.\n");
		return false;
	}
	oc->oformat = fmt;

	video_outbuf = (uint8_t *) malloc(buffsize);
	if(!video_outbuf){
		fprintf(stderr, "Could not allocate memory for video output buffer!\n");
		return false;
	}
	
    if (fmt->video_codec == CODEC_ID_NONE)
		return false;
	if(!( video_st = add_video_stream(oc, fmt->video_codec) ))
		return false;
		
    //set the output parameters (must be done even if no parameters)
    if (av_set_parameters(oc, NULL) < 0) {
        fprintf(stderr, "Invalid output format parameters\n");
        return false;
    }

    /* now that all the parameters are set, we can open the audio and
       video codecs and allocate the necessary encode buffers */
	if(!open_video(oc, video_st))
		return false;

    return true;
}

//Encode & write video frame, returns size of encoded frame
int VideoEncoder::write_video_frame(int maxSize)
{
    AVCodecContext *c;
    AVFrame *picture_ptr;
    int frameSize;
	
	if(maxSize > buffsize)
		return -1;
	
    c = video_st->codec;
	
	/* Since VNC only generate RGB picture, we must convert it
    to MJPEG's YUV pixel format */
	if(img_convert_ctx == NULL) {
	  fprintf(stderr, "Convert context NULL\n");
	  return -1;
	}
	/* @return          the height of the output slice */
	frameSize = sws_scale(img_convert_ctx, tmp_picture->data, tmp_picture->linesize,
						  0, height, picture->data, picture->linesize);
	/* continue not? */
	if(frameSize < 0)
	  return -1;

	picture_ptr = picture;

    //encode the image
    frameSize = avcodec_encode_video(c, video_outbuf, maxSize, picture_ptr);
	
	return frameSize;
}

//Get pointer to video output buffer
u_int8_t* VideoEncoder::getVideoOutputBuffer(){
	return video_outbuf;
}

//Close video
void VideoEncoder::close_video()
{
    avcodec_close(video_st->codec);
    av_free(picture->data[0]);
    av_free(picture);
    if (tmp_picture) {
        av_free(tmp_picture->data[0]);
        av_free(tmp_picture);
    }
    av_free(video_outbuf);
	
	/* free the streams */
    for(int i = 0; i < oc->nb_streams; ++i) {
        av_freep(&oc->streams[i]);
    }

    /* free the stream */
    av_free(oc);

	/* free converter */
	sws_freeContext(img_convert_ctx);
}

//Get pointer to video input buffer
u_int8_t* VideoEncoder::getVideoInputBuffer(){
	return tmp_picture->data[0];
}

int VideoEncoder::getBufferSize(){
	return buffsize;
}
