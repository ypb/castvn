/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**********/
// Choon Jin Ng
// Copyright (c) 2005 ViSLAB, University of Sydney.  All rights reserved.
// MJPEG input device
// C++ header

#ifndef _MJPEG_DEVICE_SOURCE_HH
#define _MJPEG_DEVICE_SOURCE_HH

#ifndef _JPEG_VIDEO_SOURCE_HH
#include "JPEGVideoSource.hh"
#endif

#include "VideoEncoder.hh"

extern "C"{
#include "rfb/rfbclient.h"
};

#define JPEG_HEADER_SIZE 539 //ffMpeg JPEG header size
#ifndef OUTPUT_JPG_FILE
  #define OUTPUT_JPG_FILE 0
#endif

class MJPEGDeviceSource: public JPEGVideoSource {
public:
  static MJPEGDeviceSource* createNew(UsageEnvironment& env,
					   unsigned fps, rfbClient *client, VideoEncoder *videoEncoder);

protected:
  MJPEGDeviceSource(UsageEnvironment& env,
						unsigned fps, rfbClient *client, VideoEncoder *videoEncoder);
  // called only by createNew()
  virtual ~MJPEGDeviceSource();

private:
  // redefined virtual functions:
  virtual void doGetNextFrame();
  virtual u_int8_t type();
  virtual u_int8_t qFactor();
  virtual u_int8_t width();
  virtual u_int8_t height();
  virtual u_int8_t const *quantizationTables(u_int8_t& precision, u_int16_t& length);

public:
	//video output
	u_int8_t *video_outbuf;
	int frame_count;

#if OUTPUT_JPG_FILE
	FILE *file;
#endif

private:
  void deliverFrameToClient();

private:
  rfbClient* client;
  unsigned fps;
  u_int8_t fLastWidth, fLastHeight;
  u_int8_t *qTable;
  VideoEncoder *videoEncoder;
};

#endif
