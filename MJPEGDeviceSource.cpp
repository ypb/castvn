/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**********/
// Choon Jin Ng
// Copyright (c) 2005 ViSLAB, University of Sydney.  All rights reserved.
// MJPEG input device
// Implementation

#include "MJPEGDeviceSource.hh"

extern "C" {
#include <string.h>
#include "rfb/rfbclient.h"
};

MJPEGDeviceSource*
MJPEGDeviceSource::createNew(UsageEnvironment& env,
				  unsigned fps, rfbClient *client, VideoEncoder *videoEncoder) {
  return new MJPEGDeviceSource(env, fps, client, videoEncoder);
}

MJPEGDeviceSource
::MJPEGDeviceSource(UsageEnvironment& env,
			 unsigned fps, rfbClient *client, VideoEncoder *videoEncoder)
  : JPEGVideoSource(env), fps(fps){
	  this->videoEncoder = videoEncoder;
	  this->client = client;
	  qTable = NULL;

	  frame_count = 0;
}

MJPEGDeviceSource::~MJPEGDeviceSource() {
}

//Prepare to get next frame
void MJPEGDeviceSource::doGetNextFrame() {
	/*Wait for incoming VNC socket data. Basically is a select() code.
	/ Slower refresh rate if no changes to screen */
	int i = WaitForMessage(client, 1000000 / fps);
	if(i == 0){ //If timeout, send a framebuffer update requests
		SendFramebufferUpdateRequest(client, 0, 0, client->width, client->height, FALSE);
		doGetNextFrame();
	}
	if(i < 0 ){ //Error in connection to server
		fprintf(stderr, "No incoming data!\n");
    	handleClosure(this);
    	return;	
	}
	if(i > 0) //Incoming VNC data
		deliverFrameToClient();
}

void MJPEGDeviceSource::deliverFrameToClient() {
	if(!isCurrentlyAwaitingData()) return;
	
	//Handler the VNC server message. Update the framebuffer
	if (!HandleRFBServerMessage(client)) {
		fprintf(stderr, "Error in HandleRFBServerMessage!\n");
    	handleClosure(this);
    	return;
	}
	fFrameSize = videoEncoder->write_video_frame(videoEncoder->getBufferSize());
	video_outbuf = videoEncoder->getVideoOutputBuffer();
	fFrameSize = fFrameSize - JPEG_HEADER_SIZE; //Not sending JPEG header
	
	//Get image width & height from JPEG header
	Boolean foundIt = False;
  	for (int i = 0; i < JPEG_HEADER_SIZE - 1; ++i) {
    	if (video_outbuf[i] == 0xFF && video_outbuf[i+1] == 0xC0) {
      		fLastHeight = (video_outbuf[i+5]<<5)|(video_outbuf[i+6]>>3);
      		fLastWidth = (video_outbuf[i+7]<<5)|(video_outbuf[i+8]>>3);
      		foundIt = True;
			break;
    	}
  	}
	if (!foundIt){
		fprintf(stderr, "MJPEGDeviceSource: Failed to find SOF0 marker in header!\n");
	}
	
	//Get quantization table from JPEG header
  	for (int i = 0; i < JPEG_HEADER_SIZE - 1; ++i) {
		if (video_outbuf[i] == 0xFF && video_outbuf[i+1] == 0xDB) {
			if(qTable != NULL)
				free(qTable);
			
			qTable = (u_int8_t *) malloc(sizeof(u_int8_t) * 128);
			memcpy(qTable, video_outbuf + i + 5, 64);
			memcpy(qTable + 64, video_outbuf + i + 5, 64);
		}
  	}
	
	//Copy the image to fTo buffer to be multicast
	if(fFrameSize > fMaxSize){
		fNumTruncatedBytes = fFrameSize - fMaxSize;
		fFrameSize = fMaxSize;
	}
	memmove(fTo, video_outbuf + JPEG_HEADER_SIZE, fFrameSize);
	
	// Set the 'presentation time': the time that this frame was captured
	gettimeofday(&fPresentationTime, NULL);
	
	#if OUTPUT_JPG_FILE
	//Write frame to file
	char filename[20];
	snprintf(filename, 20, "video_%06d.jpg", frame_count++);
	file = fopen(filename, "wb");
	fwrite(video_outbuf, 1, fFrameSize + JPEG_HEADER_SIZE, file);
	fclose(file);
	#endif

  	//Switch to another task, and inform the reader that he has data:
  	nextTask() = envir().taskScheduler().scheduleDelayedTask(0,
			   (TaskFunc*)afterGetting, this);
}

//JPEG frame color pixel format type
u_int8_t MJPEGDeviceSource::type() {
	return 1; //Type 1 - YUV420P
}

//JPEG frame q-factor: Anything >= 128 is custom table
u_int8_t MJPEGDeviceSource::qFactor() {
	return 195;
}

//JPEG frame width
u_int8_t MJPEGDeviceSource::width() {
	return fLastWidth; //Get frame's last width
}

//JPEG frame height
u_int8_t MJPEGDeviceSource::height() {
	return fLastHeight; //Get frame's last height
}

//Custom quantization table
u_int8_t const * MJPEGDeviceSource::quantizationTables(u_int8_t &precision, u_int16_t &length){
	//Get from last frame
	length = 0;
	precision = 0;
	
	if(qTable == NULL)
		return NULL;
	
	precision = 8;
	length = 64 * 2; //Q-table is 64-bytes.
	
	return qTable;
}
