# What is this supposed to be?

VNCast is a modified VNC Client that will receive the VNC data, compress
them into format such as motion JPEG and multicast them with RTP into the
network. Any media player that has RTP protocol support will then be able
to receive the compressed data, decode and display it.

see [VNCast](http://www.vislab.usyd.edu.au/moinwiki/VNCast) for more details

## How to build

First, on Debian-like distros:

    apt-get install liblivemedia-dev libavcodec-dev libavformat-dev libvncserver-dev
	apt-get install libswscale-dev

, then:

    make

and perhaps

    make install


## State

Well, it builds... and under certain conditions displays "something", or even,
while protesting loudly, displays something... very psychedelic...
