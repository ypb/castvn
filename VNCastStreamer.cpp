/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**********/
// Choon Jin Ng
// Copyright (c) 2005, ViSLAB, University of Sydney.  All rights reserved
// VNCast: A program that captures frames from VNC, convert them into MJPEG,
// and streams them via RTP/RTCP
// main program

#include "liveMedia.hh"
#include "GroupsockHelper.hh"

#include "BasicUsageEnvironment.hh"
#include "MJPEGDeviceSource.hh"

extern "C"{
#include "rfb/rfbclient.h"
#include <signal.h>
#include <stdio.h>
#include <string.h>
};

#define VIDEO_CODEC_MJPEG 0
#define VIDEO_CODEC_H264 1
UsageEnvironment* env;
VideoEncoder *videoEncoder;
char* progName;
char destinationAddressStr[15];
int videoCodecType;
int fps, port;
rfbClient* client;

/***********************************************************
/ VNC callback functions */
static rfbBool resize(rfbClient* client) {
	//H264 in development
	/*if(videoCodecType  == VIDEO_CODEC_MJPEG)
		videoEncoder = new VideoEncoder(200000, "mjpeg", client->width, client->height, fps);
	else if(videoCodecType == VIDEO_CODEC_H264)
		videoEncoder = new VideoEncoder(200000, "h264", client->width, client->height, fps);
	else
		videoEncoder = NULL;*/
	
	videoEncoder = new VideoEncoder(200000, "mjpeg", client->width, client->height, fps);
	
	if(videoEncoder == NULL){
		fprintf(stderr, "Couldn't find suitable codec!\n");
		exit(1);
	}
	
	if(!videoEncoder->movie_open()){
		fprintf(stderr, "Failed to initialize encoder!\n");
		exit(1);
	}
	client->frameBuffer=videoEncoder->getVideoInputBuffer();

	return TRUE;
}

static void update(rfbClient* client,int x,int y,int w,int h) {
}

void play(); // forward

void usage() {
	*env << "Usage: " << progName << " [OPTION] multicast_address:port server:screen\n";
	*env << "Options:\n-fps FPS\tFPS is frames per second. Default is 25.\n";
	//H264 still in development
	//*env << "-format FORMAT\tFORMAT is mjpeg or h264. Default is mjpeg.\n";
	*env << "\nReport bugs to <cjng@vislab.usyd.edu.au>.\n";
	exit(1);
}

int main(int argc, char** argv) {
  // Begin by setting up our usage environment:
  TaskScheduler* scheduler = BasicTaskScheduler::createNew();
  env = BasicUsageEnvironment::createNew(*scheduler);

  //Default: 25fps, mjpeg, port=2222, multicast_address = 239.255.42.42
  fps = 25;
  videoCodecType = VIDEO_CODEC_MJPEG;
  port = 2222;
  strcpy(destinationAddressStr, "239.255.42.42");
	
  progName = argv[0];
  if (argc < 2) usage();
	  
  for(int i=1; i < argc - 1; ++i){
	  if( strcmp(argv[i], "-fps") == 0 )
		  fps = atoi(argv[i+1]);
	  //H264 still in development
	  /*if( strcmp(argv[i], "-format") == 0){
		  if( strcmp(argv[i+1], "mjpeg") == 0 )
			  videoCodecType = VIDEO_CODEC_MJPEG;
		  else if( strcmp(argv[i+1], "h264") == 0)
			  videoCodecType = VIDEO_CODEC_H264;
		  else{
			  fprintf(stderr, "Video format can be only 'mjpeg' or 'h264'.\n");
			  usage();
		  }
	  }*/
  }

  if(strchr(argv[argc-2], ':') != NULL && argc > 2){
	strncpy(destinationAddressStr, strtok(argv[argc-2], ":"), 14);
  	port = atoi(strtok(NULL, ":"));
  }
		
  /* get a vnc client structure (don't connect yet). */
  client = rfbGetClient(5,3,2);
  client->format.redShift=11; client->format.redMax=31;
  client->format.greenShift=5; client->format.greenMax=63;
  client->format.blueShift=0; client->format.blueMax=31;
 
  if(!strncmp(argv[argc-1],":",1) ||
	  !strncmp(argv[argc-1],"127.0.0.1",9) ||
  	  !strncmp(argv[argc-1],"localhost",9))
	    client->appData.encodingsString="raw";
  
  /* open VNC connection */
  client->MallocFrameBuffer=resize;
  client->GotFrameBufferUpdate=update;
  
  if(!rfbInitClient(client,&argc,argv)) usage();

  if(client->serverPort==-1)
	client->vncRec->doNotSleep = TRUE; /* vncrec playback */
  
  play();
  
  delete videoEncoder;
  
  return 0;
}

void afterPlaying(void* clientData); // forward

// A structure to hold the state of the current session.
// It is used in the "afterPlaying()" function to clean up the session.
struct sessionState_t {
  FramedSource* source;
  RTPSink* sink;
  RTCPInstance* rtcpInstance;
  Groupsock* rtpGroupsock;
  Groupsock* rtcpGroupsock;
  RTSPServer* rtspServer;
} sessionState;

void play() {
  unsigned timePerFrame = 1000000/fps; // microseconds
  if(videoCodecType == VIDEO_CODEC_MJPEG){
  	sessionState.source
    	= MJPEGDeviceSource::createNew(*env, fps, client, videoEncoder);
  }
  //H264 in development
  /*if(videoCodecType == VIDEO_CODEC_H264){
	sessionState.source
        = H264DeviceSource::createNew(*env, fps, client, videoEncoder);
  }*/
	  
  if (sessionState.source == NULL) {
    *env << "Unable to open VNC Session: "
	 << env->getResultMsg() << "\n";
    exit(1);
  }

  // Create 'groupsocks' for RTP and RTCP:
  struct in_addr destinationAddress;
  destinationAddress.s_addr = our_inet_addr(destinationAddressStr);
  const unsigned short rtpPortNum = port;
  const unsigned short rtcpPortNum = rtpPortNum+1;
  const unsigned char ttl = 255;
  
  const Port rtpPort(rtpPortNum);
  const Port rtcpPort(rtcpPortNum);
  
  sessionState.rtpGroupsock
    = new Groupsock(*env, destinationAddress, rtpPort, ttl);
  sessionState.rtcpGroupsock
    = new Groupsock(*env, destinationAddress, rtcpPort, ttl);
  sessionState.rtpGroupsock->multicastSendOnly(); // we're a SSM source
  sessionState.rtcpGroupsock->multicastSendOnly(); // we're a SSM source

  // Create an appropriate RTP sink from the RTP 'groupsock':
  if(videoCodecType == VIDEO_CODEC_MJPEG){
    sessionState.sink
	  = JPEGVideoRTPSink::createNew(*env, sessionState.rtpGroupsock);
  }
  // H264 in development
  /*if(videoCodecType == VIDEO_CODEC_H264){
	sessionState.sink
	  = H264VideoRTPSink::createNew(*env, sessionState.rtpGroupsock);
  }*/
  
  // Create (and start) a 'RTCP instance' for this RTP sink:
  unsigned const averageFrameSizeInBytes = 65000; // estimate
  const unsigned totalSessionBandwidth
    = (8*1000*averageFrameSizeInBytes)/timePerFrame;
      // in kbps; for RTCP b/w share
  const unsigned maxCNAMElen = 100;
  unsigned char CNAME[maxCNAMElen+1];
  //gethostname((char*)CNAME, maxCNAMElen);
  sprintf((char*)CNAME, "VNC"); // "gethostname()" isn't supported
  CNAME[maxCNAMElen] = '\0'; // just in case
  sessionState.rtcpInstance =
    RTCPInstance::createNew(*env, sessionState.rtcpGroupsock,
			      totalSessionBandwidth, CNAME,
			      sessionState.sink, NULL /* we're a server */,
			      true /* we're a SSM source*/);
				  
  // Note: This starts RTCP running automatically
  // Create and start a RTSP server to serve this stream:
  sessionState.rtspServer
    = RTSPServer::createNew(*env, 7070);
  if (sessionState.rtspServer == NULL) {
    *env << "Failed to create RTSP server: " << env->getResultMsg() << "\n";
    exit(1);
  }
  ServerMediaSession* sms
  = ServerMediaSession::createNew(*env, NULL, progName,
	   "Session streamed by VNC", True/*SSM*/);
  sms->addSubsession(PassiveServerMediaSubsession
		    ::createNew(*sessionState.sink));
  sessionState.rtspServer->addServerMediaSession(sms);
 
  char* url = sessionState.rtspServer->rtspURL(sms);
  *env << "Play this stream using the URL \"" << url << "\"\n";
  delete[] url;
  
  // Finally, start the streaming:
  *env << "Beginning streaming...\n";
  sessionState.sink->startPlaying(*sessionState.source, afterPlaying, NULL);

  env->taskScheduler().doEventLoop();
}


void afterPlaying(void* /*clientData*/) {
  *env << "...done streaming\n";

  // End by closing the media:
  Medium::close(sessionState.rtspServer);
  Medium::close(sessionState.sink);
  delete sessionState.rtpGroupsock;
  Medium::close(sessionState.source);
  Medium::close(sessionState.rtcpInstance);
  delete sessionState.rtcpGroupsock;

  // We're done:
  exit(0);
}
